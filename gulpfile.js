var gulp          = require('gulp'),
    jshint        = require('gulp-jshint'),
    data          = require('gulp-data'),
    rename        = require('gulp-rename'),
    path          = require('path'),
    pug           = require('gulp-pug'),
    wiredep       = require('wiredep').stream,
    bower         = require('gulp-bower'),
    browserSync   = require('browser-sync').create(),
    sass          = require('gulp-sass'),
    autoprefixer  = require('gulp-autoprefixer'),
    inject        = require('gulp-inject');

// defines our default task stream
gulp.task('default', ['lint-gulp','pug', 'css', 'css-inject', 'lint-js', 'js', 'js-inject', 'bower']);

// lints our gulp file
gulp.task('lint-gulp', function() {
    return gulp.src("./gulpfile.js")
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'));
});

// Converts pug files into html
gulp.task('pug', function() {
    return gulp.src("app/pug/**/*.pug")
        .pipe(pug({pretty: true}))
        .pipe(gulp.dest("app/"))
        .pipe(browserSync.stream());
});

// Compiles & autoprefixes our css
gulp.task('css', function() {
    return gulp.src("app/sass/**/*.sass")
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(gulp.dest("app/assets/css/"))
        .pipe(browserSync.stream());
});

// injects css tags in our main html
gulp.task('css-inject', ['css'], function() {
    return gulp.src("app/*.html")
        .pipe(inject(
            gulp.src(["app/assets/css/**/*.css"], {read: false}), {relative: true}))
        .pipe(gulp.dest("app/"))
        .pipe(browserSync.stream());
});

// lints our js files before minify
gulp.task('lint-js', function() {
    return gulp.src("app/js/**/*.js")
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});

// uglify concant move to assets
gulp.task('js', ['lint-js'], function() {
    return gulp.src("app/js/**/*.js")
        .pipe(gulp.dest("app/assets/js/"))
        .pipe(browserSync.stream());
});

// inject script tags in our main html
gulp.task('js-inject', ['js', 'css-inject'], function() {
    return gulp.src("app/*.html")
        .pipe(inject(
            gulp.src(["app/assets/js/**/*.js"], {read: false}), {ignorePath: 'app/', addRootSlash: false}))
        .pipe(gulp.dest("app/"))
        .pipe(browserSync.stream());
});

// reloads the browser if js files were modified
gulp.task('js-watch', ['js-inject'], function (done) {
    browserSync.reload();
    done();
});

// inject the libraries installed with bower to our project
gulp.task('bower', ['css-inject', 'js-inject'], function() {
    return gulp.src("app/*.html")
        .pipe(wiredep())
        .pipe(gulp.dest("app/"))
        .pipe(browserSync.stream());
});

// watches our files for changes and run the coresponding task while reloading the browser
gulp.task('serve', ['lint-gulp', 'pug', 'css', 'css-inject', 'lint-js', 'js', 'js-inject', 'bower'], function() {
    // setup browserSync
    browserSync.init(
        {
            server: { baseDir: "app/" },
            socket: { domain: 'localhost:3000' }
        }
    );

    // add files to watch list
    gulp.watch("./gulpfile.js", ['lint-gulp']);
    gulp.watch("app/pug/**/*.pug", ['pug']);
    gulp.watch("app/sass/**/*.sass", ['css']);
    gulp.watch("app/assets/css/**/*.css", ['css-inject']);
    gulp.watch("app/js/**/*.js", ['lint-js', 'js']);
    gulp.watch("app/assets/js/**/*.js", ['js-watch']);
    gulp.watch("./bower.json", ['bower']);
    gulp.watch("app/*.html").on('change', browserSync.reload);

});
